Experiment Spring-OpenID-Connect
================================

----
Attention: this project use:
- Docker (for Keycloak via Testcontainer) -- so you need your docker client running when running the integration tests.
- Lombok
----


This project configure a Spring application with OpenID-Connect (for Keycloak) without any KeyCloak library!
Just simple plain Spring Security.
And this is still quite easy.

> Important updates on March 22nd, 2018:
> Thanks to valid feedback from friends and colleagues Stéphane and Jochen and in the light of the high interested in this post, I have updated the demo.
> I basically removed all the boilerplate.
> **With Spring Boot 2 final and Spring Security 5 final, you can use OAuth2 login from within a Boot-application agains Keycloak without the need of a key cloak starker or any boilerplate code.**
> All you need is a sane configuration.
https://info.michael-simons.eu/2017/12/28/use-keycloak-with-your-spring-boot-2-application/

I decided to use the plain spring way because the Keycloak library is quite huge and has a lot of dependencies.


Spring-OpenID-Connect configuration
-----------------------------------

`SecurityConfig.java`:

```
package de.humanfork.experiment.springopenidconnect;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * Protect all endpoints, except "/api/ping".
     */
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests(authorizeRequests -> authorizeRequests
                .mvcMatchers(HttpMethod.GET, "/api/ping").permitAll()
                .anyRequest().authenticated())
             .oauth2Login();
    }

}
```

`application.properties`:

(See https://docs.spring.io/spring-security/site/docs/current/reference/html5/#jc-oauth2login-client-registration-repo)

```
#run the spring application on port 8081 because Keycloak use 8080
server.port=8081

#define some properties that used in paceholders of other properties
localkeycloak.server-url=http://localhost:8080/auth
localkeycloak.realm=SpringBootKeycloak
localkeycloak.openid-connect-url=${localkeycloak.server-url}/realms/${localkeycloak.realm}/protocol/openid-connect

# the "localkeycloak" part of the messagekey is just a name one can choose free
spring.security.oauth2.client.registration.localkeycloak.client-id=springboot-login-app
spring.security.oauth2.client.registration.localkeycloak.client-secret=5f9f12c5-8955-4f21-a63a-b7d343047448
spring.security.oauth2.client.registration.localkeycloak.client-name=some client
spring.security.oauth2.client.registration.localkeycloak.scope=openid, profile
#the default redirect URI template is {baseUrl}/login/oauth2/code/{registrationId}.
#The registrationId is a unique identifier for the ClientRegistration. 
spring.security.oauth2.client.registration.localkeycloak.redirect-uri={baseUrl}/login/oauth2/code/{registrationId}
spring.security.oauth2.client.registration.localkeycloak.authorization-grant-type=authorization_code

spring.security.oauth2.client.provider.localkeycloak.token-uri=${localkeycloak.openid-connect-url}/token
spring.security.oauth2.client.provider.localkeycloak.authorization-uri=${localkeycloak.openid-connect-url}/auth
spring.security.oauth2.client.provider.localkeycloak.user-info-uri=${localkeycloak.openid-connect-url}/userinfo
spring.security.oauth2.client.provider.localkeycloak.jwk-set-uri=${localkeycloak.openid-connect-url}/certs
spring.security.oauth2.client.provider.localkeycloak.user-name-attribute=preferred_username
```

----
Hint2:
To create a Keycloak client secret (`spring.security.oauth2.client.registration.localkeycloak.client-secret`)
one must set (in Keycloak) the "Access Type" for the client to "confidential".
Then a new tab "credentials" will sporn. [Do Keycloak Clients have a Client Secret](https://stackoverflow.com/questions/44752273/do-keycloak-clients-have-a-client-secret)

----

----
Hint: there are already build on configurations for some OpenID Connect provides.
To activat it one has to set the property `provider`, for example to "google"
When using them one can drop the most of the configuration, exept `client-id` and `client-secret`.

```
spring.security.oauth2.client.registration.example.provider=google
spring.security.oauth2.client.registration.example.client-id=my-google-client
spring.security.oauth2.client.registration.example.client-secret=password
```
---- 

Keycloak
--------
See `keycloak-config.md` for configuration details.
The integration set up its own instance of Keycloak

Integration Tests
-----------------
Integration Tests (`AppIT.java`) use [Testcontainers](https://www.testcontainers.org/) to fire up a Keycloak instance via Docker. 

Used KeyCloak - Testcontainer is: Keycloak Testcontainer https://github.com/dasniko/testcontainers-keycloak
```			
<dependency>
    <groupId>com.github.dasniko</groupId>
    <artifactId>testcontainers-keycloak</artifactId>
    <version>1.5.0</version>
</dependency>
```
see: [A Keycloak Testcontainer](https://www.n-k.de/2019/12/testcontainers-keycloak.html)
and [Keycloak SAML Integration Test mit Testcontainers und Spring Boot](https://www.trion.de/news/2020/06/15/keycloak-saml-integration-test-testcontainers.html) 

The used Keycloak configuration is `src/test/resources/realm-export.json` (contain the realm configuration and the user data).

Lombok
------

This project use [Project Lombok](https://projectlombok.org/) therefore you need to have Lombok installed

### Lombok - Eclipse - Overlapping text edits bug
There is one problem with Eclipse: The cleanup setting with "Code Style / Use parentheses in expressions".
If it is enabled, then Eclipse cleanup will fail for `@Data` annotated classes:
>An unexpected exception occured while performing the refactoring.
>
>Overlappping text edits
https://bugs.eclipse.org/bugs/show_bug.cgi?id=535536


Resources
---------

* https://info.michael-simons.eu/2017/12/28/use-keycloak-with-your-spring-boot-2-application/

* https://docs.spring.io/spring-security/site/docs/current/reference/html5/#oauth2
* https://spring.io/guides/tutorials/spring-boot-oauth2/
* https://piotrminkowski.com/2020/10/09/spring-cloud-gateway-oauth2-with-keycloak/

* https://www.baeldung.com/spring-boot-keycloak
* https://www.baeldung.com/spring-security-openid-connect

* https://openid.net/connect/
* https://auth0.com/resources/ebooks/the-openid-connect-handbook
* https://m.heise.de/developer/artikel/OpenID-Connect-Login-mit-OAuth-Teil-1-Grundlagen-2218446.html?seite=all
* https://m.heise.de/developer/artikel/OpenID-Connect-Login-mit-OAuth-Teil-2-Identity-Federation-und-fortgeschrittene-Themen-2266017.html?seite=all
