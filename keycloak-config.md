Keycloak configuration
======================

Keycloak version: 12.0.3

realm: `SpringBootKeycloak`
---------------------------
- Enabled: true
- Endpoints: `OpenID Endpoint Configuration`


### Client "springboot-login-app"
- Client ID: `springboot-login-app`
- Enabled: true
- Client Protocol `openid-connect`
- Access Type `confidential` --> after enabling that property and saving the settings, a new tab "Credentials" shows up
- Standard Flow Enabled: true
- Valid Redirect URIs: `http://localhost:8081/*`

### Roles "user"
- Role Name `user`

### Users "user2"
- username: `user2`
- enabled: true
- credentials/password: `geheim`
- role Mappings/assigned Roles: `offline_access`, `uma_authorization`, `user`


### Export

The file `src/test/resources/realm-export.json` contain the realm configuration and the user data.

It was exported by this command
```
standalone.bat
   -Dkeycloak.migration.action=export
   -Dkeycloak.migration.provider=singleFile
   -Dkeycloak.migration.file=c:\temp\keycloak\realm-export.json
   -Dkeycloak.migration.realmName=SpringBootKeycloak
```
so it contains the user data and credentials, that would not be contained if the export is done from the admin console.



Appendix
--------

### realm "OpenID Endpoint Configuration"
```
{
	"issuer" : "http://localhost:8080/auth/realms/SpringBootKeycloak",
	"authorization_endpoint" : "http://localhost:8080/auth/realms/SpringBootKeycloak/protocol/openid-connect/auth",
	"token_endpoint" : "http://localhost:8080/auth/realms/SpringBootKeycloak/protocol/openid-connect/token",
	"introspection_endpoint" : "http://localhost:8080/auth/realms/SpringBootKeycloak/protocol/openid-connect/token/introspect",
	"userinfo_endpoint" : "http://localhost:8080/auth/realms/SpringBootKeycloak/protocol/openid-connect/userinfo",
	"end_session_endpoint" : "http://localhost:8080/auth/realms/SpringBootKeycloak/protocol/openid-connect/logout",
	"jwks_uri" : "http://localhost:8080/auth/realms/SpringBootKeycloak/protocol/openid-connect/certs",
	"check_session_iframe" : "http://localhost:8080/auth/realms/SpringBootKeycloak/protocol/openid-connect/login-status-iframe.html",
	"grant_types_supported" : ["authorization_code", "implicit", "refresh_token", "password", "client_credentials"],
	"response_types_supported" : ["code", "none", "id_token", "token", "id_token token", "code id_token", "code token", "code id_token token"],
	"subject_types_supported" : ["public", "pairwise"],
	"id_token_signing_alg_values_supported" : ["PS384", "ES384", "RS384", "HS256", "HS512", "ES256", "RS256", "HS384", "ES512", "PS256", "PS512", "RS512"],
	"id_token_encryption_alg_values_supported" : ["RSA-OAEP", "RSA-OAEP-256", "RSA1_5"],
	"id_token_encryption_enc_values_supported" : ["A256GCM", "A192GCM", "A128GCM", "A128CBC-HS256", "A192CBC-HS384", "A256CBC-HS512"],
	"userinfo_signing_alg_values_supported" : ["PS384", "ES384", "RS384", "HS256", "HS512", "ES256", "RS256", "HS384", "ES512", "PS256", "PS512", "RS512", "none"],
	"request_object_signing_alg_values_supported" : ["PS384", "ES384", "RS384", "HS256", "HS512", "ES256", "RS256", "HS384", "ES512", "PS256", "PS512", "RS512", "none"],
	"response_modes_supported" : ["query", "fragment", "form_post"],
	"registration_endpoint" : "http://localhost:8080/auth/realms/SpringBootKeycloak/clients-registrations/openid-connect",
	"token_endpoint_auth_methods_supported" : ["private_key_jwt", "client_secret_basic", "client_secret_post", "tls_client_auth", "client_secret_jwt"],
	"token_endpoint_auth_signing_alg_values_supported" : ["PS384", "ES384", "RS384", "HS256", "HS512", "ES256", "RS256", "HS384", "ES512", "PS256", "PS512", "RS512"],
	"claims_supported" : ["aud", "sub", "iss", "auth_time", "name", "given_name", "family_name", "preferred_username", "email", "acr"],
	"claim_types_supported" : ["normal"],
	"claims_parameter_supported" : true,
	"scopes_supported" : ["openid", "address", "email", "microprofile-jwt", "offline_access", "phone", "profile", "roles", "web-origins"],
	"request_parameter_supported" : true,
	"request_uri_parameter_supported" : true,
	"require_request_uri_registration" : true,
	"code_challenge_methods_supported" : ["plain", "S256"],
	"tls_client_certificate_bound_access_tokens" : true,
	"revocation_endpoint" : "http://localhost:8080/auth/realms/SpringBootKeycloak/protocol/openid-connect/revoke",
	"revocation_endpoint_auth_methods_supported" : ["private_key_jwt", "client_secret_basic", "client_secret_post", "tls_client_auth", "client_secret_jwt"],
	"revocation_endpoint_auth_signing_alg_values_supported" : ["PS384", "ES384", "RS384", "HS256", "HS512", "ES256", "RS256", "HS384", "ES512", "PS256", "PS512", "RS512"],
	"backchannel_logout_supported" : true,
	"backchannel_logout_session_supported" : true
}

```


### client: `springboot-login-app` export
```
{
    "clientId": "springboot-login-app",
    "adminUrl": "",
    "baseUrl": "",
    "surrogateAuthRequired": false,
    "enabled": true,
    "alwaysDisplayInConsole": false,
    "clientAuthenticatorType": "client-secret",
    "redirectUris": [
        "",
        "http://localhost:8081/*"
    ],
    "webOrigins": [
        ""
    ],
    "notBefore": 0,
    "bearerOnly": false,
    "consentRequired": false,
    "standardFlowEnabled": true,
    "implicitFlowEnabled": false,
    "directAccessGrantsEnabled": true,
    "serviceAccountsEnabled": false,
    "publicClient": false,
    "frontchannelLogout": false,
    "protocol": "openid-connect",
    "attributes": {
        "saml.assertion.signature": "false",
        "saml.force.post.binding": "false",
        "saml.multivalued.roles": "false",
        "saml.encrypt": "false",
        "backchannel.logout.revoke.offline.tokens": "false",
        "saml.server.signature": "false",
        "saml.server.signature.keyinfo.ext": "false",
        "exclude.session.state.from.auth.response": "false",
        "backchannel.logout.session.required": "true",
        "client_credentials.use_refresh_token": "false",
        "saml_force_name_id_format": "false",
        "saml.client.signature": "false",
        "tls.client.certificate.bound.access.tokens": "false",
        "saml.authnstatement": "false",
        "display.on.consent.screen": "false",
        "saml.onetimeuse.condition": "false"
    },
    "authenticationFlowBindingOverrides": {},
    "fullScopeAllowed": true,
    "nodeReRegistrationTimeout": -1,
    "defaultClientScopes": [
        "web-origins",
        "role_list",
        "roles",
        "profile",
        "email"
    ],
    "optionalClientScopes": [
        "address",
        "phone",
        "offline_access",
        "microprofile-jwt"
    ],
    "access": {
        "view": true,
        "configure": true,
        "manage": true
    }
}
```
