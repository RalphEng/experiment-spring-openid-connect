package de.humanfork.experiment.springopenidconnect;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * Protect all endpoints, except "/api/ping".
     */
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.authorizeRequests(authorizeRequests -> authorizeRequests
                .mvcMatchers(HttpMethod.GET, "/api/ping").permitAll()
                .anyRequest().authenticated())        
            .oauth2Login();
    }

}
