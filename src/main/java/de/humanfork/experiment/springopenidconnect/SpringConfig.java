package de.humanfork.experiment.springopenidconnect;

import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.humanfork.experiment.springopenidconnect.dao.CustomerRepository;
import de.humanfork.experiment.springopenidconnect.domain.Customer;

@Configuration
public class SpringConfig {

    @Bean
    public CommandLineRunner init(final CustomerRepository customerRepository) {
        return args -> {
            List.of("ralph", "sabrina", "benjamin", "sophie").stream()
                    .map(Customer::new)
                    .forEach(customerRepository::save);
        };
    }
}
