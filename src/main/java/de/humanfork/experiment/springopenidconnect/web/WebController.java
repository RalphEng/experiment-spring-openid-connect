package de.humanfork.experiment.springopenidconnect.web;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import de.humanfork.experiment.springopenidconnect.dao.CustomerRepository;
import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor
public class WebController {

    private final CustomerRepository customerRepository;

    @GetMapping(path = "/")
    public ModelAndView index() {
        return new ModelAndView("index");
    }

    @GetMapping(path = "/customers")
    public ModelAndView customers(final Principal principal) {
        ModelMap model = new ModelMap();
        model.addAttribute("customers", this.customerRepository.findAll());
        model.addAttribute("username", principal.getName());
        return new ModelAndView("customers", model);
    }
}
