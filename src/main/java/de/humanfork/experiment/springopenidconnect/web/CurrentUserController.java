package de.humanfork.experiment.springopenidconnect.web;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/currentUser")
public class CurrentUserController {

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    @Getter
    public static final class PrincipalAbstract {
        private final String name;

        private final Collection<String> authorityNames;

        public static PrincipalAbstract from(@NonNull OidcUser principal) {
            return new PrincipalAbstract(principal.getName(),
                    principal.getAuthorities().stream()
                            .map(GrantedAuthority::getAuthority)                    
                            .collect(Collectors.toList()));
        }
    }

    @GetMapping("/authenticationPrincipal")
    public PrincipalAbstract authenticationPrincipal(@AuthenticationPrincipal OidcUser principal) {
        Objects.requireNonNull(principal);

        System.out.println("@AuthenticationPrincipal OidcUser principal:");
        System.out.println(" class   : " + principal.getClass());
        System.out.println(" toString: " + principal);
        System.out.println(" name            : " + principal.getName());
        System.out.println(" nickName        : " + principal.getNickName());
        System.out.println(" preferredUsernam: " + principal.getPreferredUsername());
        System.out.println(" claims: "
                + principal.getClaims().entrySet().stream()
                        .map(entry -> entry.getKey() + " \t: " + entry.getValue())
                        .collect(Collectors.joining("\n -", "\n -", "")));
        System.out.println(" authorities: " + principal.getAuthorities());
        System.out.println(" userInfo.toString: " + principal.getUserInfo());

        return PrincipalAbstract.from(principal);
    }
}
