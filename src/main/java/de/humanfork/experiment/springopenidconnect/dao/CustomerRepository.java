package de.humanfork.experiment.springopenidconnect.dao;

import org.springframework.data.repository.CrudRepository;

import de.humanfork.experiment.springopenidconnect.domain.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

}
