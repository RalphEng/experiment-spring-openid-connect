package de.humanfork.experiment.springopenidconnect;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class SpringContextTest {

    @Autowired
    private ApplicationContext applicationContext;

    @Test
    public void testSpringContextLoad() {
        assertThat(this.applicationContext).isNotNull();
    }
}
