package de.humanfork.experiment.springopenidconnect;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import com.jayway.jsonpath.JsonPath;

import dasniko.testcontainers.keycloak.KeycloakContainer;
import us.codecraft.xsoup.XPathEvaluator;
import us.codecraft.xsoup.Xsoup;

/**
 * Test {@code App}.
 */
@Testcontainers
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@AutoConfigureMockMvc
public class AppIT {

    @LocalServerPort
    private int springServerPort;

    /**
     * Instantiate a KeyCloak Docker container (Keycloak version 12.0.3)
     * This variable is static to reuse the same container in different tests.
     */
    @Container
    private static final KeycloakContainer keycloak = new KeycloakContainer("quay.io/keycloak/keycloak:12.0.3")
            .withRealmImportFile("keycloak/realm-export.json")
            .waitingFor(Wait.forLogMessage(".*Keycloak .* started in \\d+ms - Started.*", 1))
            .withStartupTimeout(Duration.ofMinutes(1));

    @DynamicPropertySource
    static void dataSourceProperties(final DynamicPropertyRegistry registry) {
        registry.add("localkeycloak.server-url", () -> keycloak.getAuthServerUrl());
    }

    @Autowired
    private Environment environment;

    /** Check that the placeholders in the properties are replaced. */
    @Test
    public void testConfiguration() {
        assertThat(this.environment.getProperty("localkeycloak.server-url")).isEqualTo(keycloak.getAuthServerUrl());
        assertThat(this.environment.getProperty("localkeycloak.openid-connect-url"))
                .isEqualTo(keycloak.getAuthServerUrl() + "/realms/SpringBootKeycloak/protocol/openid-connect");
    }

    @Test
    public void testPingSpringBootApp() throws IOException {
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().disableRedirectHandling().build()) {
            try (CloseableHttpResponse httpResponse = httpClient
                    .execute(new HttpGet("http://localhost:" + this.springServerPort + "/api/ping"))) {
                String body = EntityUtils.toString(httpResponse.getEntity());

                printResponseHeader(httpResponse);

                assertThat(httpResponse.getStatusLine().getStatusCode()).isEqualTo(200);
                assertThat(body).isEqualTo("pong");
            }
        }
    }

    @Test
    public void testPingKeycloak() throws IOException {
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().disableRedirectHandling().build()) {
            try (CloseableHttpResponse httpResponse = httpClient
                    .execute(new HttpGet(keycloak.getAuthServerUrl() + "/"))) {
                assertThat(httpResponse.getStatusLine().getStatusCode()).isEqualTo(200);
            }
        }
    }

    /** Check that the realm: SpringBootKeycloak exists on Keycloak. */
    @Test
    public void testPingKeycloakRealm() throws IOException {
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().disableRedirectHandling().build()) {
            try (CloseableHttpResponse httpResponse = httpClient
                    .execute(new HttpGet(keycloak.getAuthServerUrl() + "/realms/SpringBootKeycloak"))) {
                assertThat(httpResponse.getStatusLine().getStatusCode()).isEqualTo(200);
            }
        }
    }

    /**
     * Scenario: Login via Keycloak and then check the principal
     * @throws IOException no excption expected
     */
    @Test
    public void testLoggedIn() throws IOException {

        String applicationBaseUrl = "http://localhost:" + this.springServerPort;
        
        String authenticationPrincipalUrl = applicationBaseUrl + "/api/currentUser/authenticationPrincipal";
        String springOauthEntryPoint = applicationBaseUrl + "/oauth2/authorization/localkeycloak";
        String redirectUri = applicationBaseUrl + "/login/oauth2/code/localkeycloak";

        String keyCloakLoginAuthorizationUri = keycloak.getAuthServerUrl()
                + "/realms/SpringBootKeycloak/protocol/openid-connect/auth";

        /* xpath expression to extract the html title */
        final XPathEvaluator titleXpath = Xsoup.compile("/html/head/title/text()");

        try (CloseableHttpClient httpClient = HttpClientBuilder.create()
                .setRedirectStrategy(new LaxRedirectStrategy()) //needed to instruct http client to follow redirects even for POST requests
                .build()) {

            HttpClientContext context = HttpClientContext.create();

            String loginSubmitUrl;
            /* 1. Request: access protected resource and get redirected to Keycloak Login Page*/
            try (CloseableHttpResponse httpResponse = httpClient.execute(new HttpGet(authenticationPrincipalUrl),
                    context)) {
                                
//                printResponseHeader(httpResponse);
//                context.getRedirectLocations().forEach(System.out::println);
                assertThat(httpResponse.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.OK.value());

                /* assert that
                 * - the 1. redirect was to the central SpringBootApplication authentication handler: springOauthEntryPoint
                 * - the 2. redirect was to Keycloak: keyCloakLoginAuthorizationUri
                 */
                assertThat(sanatizeRedirectLocations(context.getRedirectLocations()).stream()
                        .map(uri -> StringUtils.substringBefore(uri, "?")))
                                .containsExactly(springOauthEntryPoint, keyCloakLoginAuthorizationUri);

                /* assert that the key cloak sign in dialog shows up (because the user is not logged in) */
                Document bodyDoc = Jsoup.parse(EntityUtils.toString(httpResponse.getEntity()));
                assertThat(titleXpath.evaluate(bodyDoc).get()).isEqualTo("Sign in to SpringBootKeycloak");

                /* extract the url for login from submit */
                loginSubmitUrl = Xsoup.compile("//form[@id='kc-form-login']/@action").evaluate(bodyDoc).get();
                assertThat(loginSubmitUrl).isNotNull().isNotBlank().startsWith(keycloak.getAuthServerUrl());
            }

            /* 2. Request: submit sign in to Keycloak and become redirect to SpringApplication */
            try (CloseableHttpResponse httpResponse = httpClient.execute(RequestBuilder
                    .post(loginSubmitUrl)
                    .addParameter("username", "user2")
                    .addParameter("password", "geheim").build(), context)) {
                assertThat(httpResponse.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.OK.value());

                /* assert that
                 * - the 1. redirect was back to to the central SpringBootApplication authentication handler: redirectUri
                 * - the 2. redirect the the primary requested resource: customersUrl
                 */
                assertThat(sanatizeRedirectLocations(context.getRedirectLocations()).stream()
                        .map(uri -> StringUtils.substringBefore(uri, "?")))
                                .containsExactly(redirectUri, authenticationPrincipalUrl);

                String jsonBody = EntityUtils.toString(httpResponse.getEntity());

                assertThat(JsonPath.<String> read(jsonBody, "$.name")).isEqualTo("user2");
                assertThat(JsonPath.<List<String>> read(jsonBody, "$.authorityNames"))
                        .containsExactlyInAnyOrder("ROLE_USER", "SCOPE_email", "SCOPE_openid", "SCOPE_profile");
            }

        }
    }

    private void printResponseHeader(final HttpResponse httpResponse) {
        Objects.requireNonNull(httpResponse);

        System.out.println(httpResponse.getStatusLine());
        Stream.of(httpResponse.getAllHeaders()).forEach(System.out::println);
    }

    private List<String> sanatizeRedirectLocations(@Nullable final List<URI> original) {
        if (original == null) {
            return Collections.emptyList();
        } else {
            return original.stream()
                    .map(URI::toString)
                    .collect(Collectors.toList());
        }
    }

}
